package de.ilem0n.sessions;

import de.ilem0n.SessionInfoReply;
import de.ilem0n.SessionRequest;
import de.ilem0n.SessionServiceInterface;
import io.quarkus.grpc.GrpcClient;
import io.quarkus.logging.Log;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/hello")
public class FlinkSessionReconciler {

    @GrpcClient
    SessionServiceInterface sessionService;

    @GET
    public Response reconcile() {
        SessionRequest sessionRequest = SessionRequest.newBuilder()
            .setSessionId("BARFOO")
            .build();
        SessionInfoReply upstreamSessionInfo = sessionService
            .getSessionInfo(sessionRequest)
            .await()
            .indefinitely();

        Log.info(String.format("UPSTREAM: %s", upstreamSessionInfo.getIsHealthy()));
        return Response.ok().build();
    }
}
