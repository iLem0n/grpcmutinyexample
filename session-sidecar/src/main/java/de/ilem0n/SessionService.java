package de.ilem0n;

import io.quarkus.grpc.GrpcService;
import io.smallrye.mutiny.Uni;

@GrpcService
public class SessionService implements SessionServiceInterface {

    @Override
    public Uni<SessionInfoReply> getSessionInfo(SessionRequest request) {
        return Uni.createFrom().item(() ->
            SessionInfoReply.newBuilder()
                .setIsHealthy(false)
                .build()
            );
    }


}
