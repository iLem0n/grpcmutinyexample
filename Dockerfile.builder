FROM jenkinsci/jnlp-slave
ARG version=1.0.0

# --- command: docker build -f ./Dockerfile -t jnlp-agent-graalvm .
LABEL Author="CK Gan" Email="chengkuan@gmail.com" Description="Jenkins jnlp agent with Graalvm." Version="$version"

USER root

ENV GRAALVM_VERSION=21.3.0
ENV GRAALVM_BASE /opt/graalvm
ENV GRAALVM_HOME $GRAALVM_BASE/graalvm-ce-java17-$GRAALVM_VERSION
ENV JAVA_HOME $GRAALVM_HOME
ENV PATH $JAVA_HOME/bin:$PATH

# --- Install GraalVM & native-image
RUN mkdir -pv $GRAALVM_BASE && \
    curl -o $GRAALVM_BASE/graalvm-ce-java17-linux-amd64-$GRAALVM_VERSION.tar.gz -L https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-$GRAALVM_VERSION/graalvm-ce-java17-linux-amd64-$GRAALVM_VERSION.tar.gz && \
    cd $GRAALVM_BASE && \
    tar xvf $GRAALVM_BASE/graalvm-ce-java17-linux-amd64-$GRAALVM_VERSION.tar.gz --directory $GRAALVM_BASE && \
    $GRAALVM_HOME/bin/gu install native-image

# ---
RUN curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

# --- install build essentails for GraalVM Native build
RUN apt-get -y update && \
    apt-get -y install build-essential docker.io  && \
    apt-get clean